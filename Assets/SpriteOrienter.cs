﻿using UnityEngine;
using System.Collections;

public class SpriteOrienter : MonoBehaviour {

	public int direction;
	private Vector3 target;
	private Animator anim;

	void Start () {
		anim = gameObject.GetComponent<Animator>();
	}

	void Update () {
		target=Camera.main.ScreenToWorldPoint (Input.mousePosition);
		setDirection();
		anim.SetInteger("dir",direction);
	}

	void setDirection(){
		if((FacingRight()==true)&&(Mathf.Abs(target.x-transform.position.x)>Mathf.Abs(target.y-transform.position.y)))
			direction = 6;
		if ((FacingLeft()==true)&&(Mathf.Abs(target.x-transform.position.x)>Mathf.Abs(target.y-transform.position.y)))
			direction = 4;
		if((FacingUp()==true)&&(Mathf.Abs(target.x-transform.position.x)<Mathf.Abs(target.y-transform.position.y)))
			direction = 8;  
		if((FacingDown()==true)&&(Mathf.Abs(target.x-transform.position.x)<Mathf.Abs(target.y-transform.position.y)))
			direction = 2;
		if((isDiagonal()==true)&&(FacingDown()==true)&&(FacingLeft()==true))
			direction=1;
		if(isDiagonal()==true&&FacingDown()==true&&FacingRight()==true)
			direction=3;
		if(isDiagonal()==true&&FacingUp()==true&&FacingRight()==true)
			direction=9;
		if(isDiagonal()==true&&FacingUp()==true&&FacingLeft()==true)
			direction=7;
	}

	bool FacingRight(){
		if(target.x-transform.position.x>0)
			return true;
		else
			return false;
	}

	bool FacingLeft(){
		if(target.x-transform.position.x<0)
			return true;
		else
			return false;
	}

	bool FacingUp(){
		if(target.y-transform.position.y>0)
			return true;
		else
			return false;

	}

	bool FacingDown(){
		if(target.y-transform.position.y<0)
			return true;
		else
			return false;
	}

	bool isDiagonal(){
		if((Mathf.Abs(Mathf.Abs(target.x-transform.position.x)-Mathf.Abs (target.y-transform.position.y)))<=0.4){
			return true;
		}
		else
		return false;
	}

}
