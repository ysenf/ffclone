﻿using UnityEngine;
using System.Collections;

public class Upgrade : MonoBehaviour {
	public enum Type {Speed, Jack, Strength, Rate};
	public Type type;
	public enum StrengthLevel {Weak = 1, Strong = 2}
	public StrengthLevel strength;
	public bool isTimed;

	protected virtual void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Player")){
		switch (type){
		case Type.Jack:
			break;
		case Type.Rate:
				RateUp(other);
			break;
		case Type.Speed:
			SpeedUp(other);
			break;
		case Type.Strength:
			StrengthUp(other);
			break;
		}
		}
	}

	void JackUp(Collider2D other){

	}

	void RateUp(Collider2D other){
		CtrWepHandler ctr = other.GetComponent<CtrWepHandler>();
		switch (isTimed){
		case true:
			ctr.TempRateUp((int)strength);
			Destroy(gameObject);
			break;
		case false:
			if(!ctr.IsRateMaxed)
			ctr.LevelRateUp((int)strength);
			Destroy(gameObject);
			break;
		}
	}

	void SpeedUp(Collider2D other){
		CtrMovement ctr = other.GetComponent<CtrMovement>();
			switch (isTimed){
			case true:
				ctr.TempLevelUp((int)strength);
				Destroy(gameObject);
				break;
			case false:
				if(!ctr.IsMaxed)
					ctr.LevelUp((int)strength);
				Destroy(gameObject);
				break;
			}
		}


	void StrengthUp(Collider2D other){
		CtrWepHandler ctr = other.GetComponent<CtrWepHandler>();
		switch (isTimed){
		case true:
			ctr.TempWeaponUp((int)strength);
			Destroy(gameObject);
			break;
		case false:
			if(!ctr.IsWeaponMaxed)
				ctr.LevelWeaponUp((int)strength);
			Destroy(gameObject);
			break;
		}
	}
}
