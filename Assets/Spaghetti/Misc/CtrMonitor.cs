﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CtrMonitor : MonoBehaviour {
	public List<Sprite> sprites;

	void Start () {
	}

	void Update () {
	
	}

	public void UpdateUI(int health){
		if(health<0)
			gameObject.GetComponent<Image>().enabled = false;
		else
			gameObject.GetComponent<Image>().sprite = sprites[health];
	}
}
