﻿using UnityEngine;
using System.Collections;

public class lineDrawing : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var cursPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		LineRenderer lineRenderer = gameObject.GetComponent("LineRenderer") as LineRenderer;
		lineRenderer.SetPosition (1, new Vector3 (gameObject.transform.position.x,gameObject.transform.position.y,0));
		lineRenderer.SetPosition (0, cursPos);
	
	}
}
