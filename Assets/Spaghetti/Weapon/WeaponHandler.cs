﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponHandler : MonoBehaviour {
	private Transform lockOn;
	public GameObject startWeapon;
	public float startRate;
	public float handOffset;
	public GameObject hand;
	protected List<Transform> weaponSpawn;
	protected float cooldown;
	protected GameObject weapon;
	public GameObject Weapon
	{
		get
		{	
			return weapon;
		}
		set
		{
			weapon = value;
		}
	}
	protected float fireRate;
	public float FireRate
	{
		get
		{	
			return fireRate;
		}
		set
		{
			fireRate = value;
		}
	}

	protected Vector3 target;
	public Vector3 Target
	{
		get
		{	
			return target;
		}
		set
		{
			target = value;
		}
	}

	protected virtual void UpdateTarget(){
	}
	protected void FireBullets(){
		foreach (Transform child in transform){ //aim
			if (child.CompareTag("hand")){
				child.rotation = GetTargetRot(transform,-90);}
		}
		foreach (Transform spawn in weaponSpawn){ //fire
			GameObject wep = (GameObject) Instantiate(weapon,spawn.position,GetTargetRot(spawn, 0));
			Bullet bullet = wep.GetComponent<Bullet>();
			bullet.Target=target;
		}
	}

	protected virtual void Initialize(){
		target=new Vector3 (9999f,9999f);
		UpdateTarget();
		fireRate=startRate;
		weapon = startWeapon;
		GameObject arm = (GameObject) Instantiate(hand,transform.position,Quaternion.identity);
		arm.transform.SetParent(transform);
		ArmWeapon (weapon.GetComponent<Bullet>().spawns);
	}

	//ArmWeapon updates the bullet spawn formation. You'd pass weapon.getcomponent<bullet>().Spawns or something like that.
	       //The reason it doesn't just use the member var for weapon is because it's supposed to work with upgrades as well. Maybe I should overload instead.
	protected void ArmWeapon(GameObject spawn){
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);  //get rid of descendants
		}
		GameObject arm = (GameObject) Instantiate(hand,transform.position,Quaternion.identity); //equip new hand
		arm.transform.SetParent(transform);
		GameObject preSpawn = (GameObject) Instantiate(spawn, new Vector3 (transform.position.x, (transform.position.y+handOffset), transform.position.z),Quaternion.identity);
		preSpawn.transform.SetParent(arm.transform); //equip weapon's spawn pattern
		List<Transform> spawnTrans = new List<Transform>();
		foreach (Transform children in arm.transform){ // put all spawns into spawn list for firing
			foreach (Transform 	gchild in children){
				spawnTrans.Add(gchild.transform);
			}
			spawnTrans.Add(children.transform);
		}
		weaponSpawn=spawnTrans;
		Debug.Log (weaponSpawn.Count);
	}	      


	protected Quaternion GetTargetRot(Transform spawn, int mod){ 
		Vector3 tarPos = Camera.main.WorldToScreenPoint(target);
		tarPos.z = 0;
		Vector3 spawnPos = Camera.main.WorldToScreenPoint(spawn.position);
		tarPos.x = tarPos.x - spawnPos.x;
		tarPos.y = tarPos.y - spawnPos.y;
		float angle = Mathf.Atan2(tarPos.y, tarPos.x) * Mathf.Rad2Deg;
		return Quaternion.Euler(new Vector3(0, 0, angle+mod));
	}

	protected virtual void WeaponUpdates(){
		UpdateTarget();
		cooldown+=Time.deltaTime;
		if(cooldown>=fireRate){
			FireBullets();
			cooldown=0f;
		}
	}

	void Update(){
		WeaponUpdates();
	}

	void Start(){
		Initialize();
	}
}
	