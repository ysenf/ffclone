﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public int ID;
	public int damage;
	public GameObject spawns;
	public float speed;
	protected Vector3 target;
	public Vector3 Target
	{
		get
		{	
			return target;
		}
		set
		{
			target = value;
		}
	}

	void Start () {
		target = target-transform.position;
		target.Normalize();
		Destroy(gameObject, speed/2f);
	}

	void Update () {
		transform.position+=(target*speed*Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D other){
		if((gameObject.CompareTag("fbullet")==true)&&(other.CompareTag("Player")==true)){
			Destroy(gameObject);
		}
		else if((gameObject.CompareTag("pbullet")==true)&&(other.CompareTag("foe")==true)){
			Destroy(gameObject);
		}
	}
}
