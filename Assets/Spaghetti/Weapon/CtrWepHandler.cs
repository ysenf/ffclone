﻿using UnityEngine;
using System.Collections;

public class CtrWepHandler : WeaponHandler {
	private Transform lockOn;
	private Vector3 dir;
	private WeaponDatabase datab;
	public int weaponCap;
	public float rateCap;
	public int rateUpgradeMax;
	public int weaponUpgradeMax;
	protected float rateDiference;
	protected int weaponDifference;
	protected bool isRateMaxed;
	public bool IsRateMaxed
	{
		get
		{	
			return isRateMaxed;
		}
		set
		{
			isRateMaxed = value;
		}
	}
	protected bool isWeaponMaxed;
	public bool IsWeaponMaxed
	{
		get
		{	
			return isWeaponMaxed;
		}
		set
		{
			isWeaponMaxed = value;
		}
	}
	protected int weaponID;
	public int WeaponID
	{
		get
		{	
			return weaponID;
		}
		set
		{
			weaponID = value;
		}
	}
	protected int tempWeaponID;
	public int TempWeaponID
	{
		get
		{	
			return tempWeaponID;
		}
		set
		{
			tempWeaponID = value;
		}
	}
	protected float tempRate;
	public float TempRate
	{
		get
		{	
			return tempRate;
		}
		set
		{
			tempRate = value;
		}
	}

	protected override void WeaponUpdates ()
	{
		UpdateTarget();
		cooldown+=Time.deltaTime;
		if(cooldown>=(Mathf.Clamp(fireRate+tempRate,startRate,rateCap))){
			FireBullets();
			cooldown=0f;
		}
	}

	protected override void UpdateTarget(){
		if(lockOn)
		dir = target-transform.position;
		else if(!lockOn)
		dir = (new Vector3 (9999999999f,999999999f))-transform.position;

		foreach (Collider2D obj in Physics2D.OverlapCircleAll(transform.position,1000f)){
			Vector3 odir = obj.transform.position-transform.position;
			if((obj.transform.CompareTag("foe")==true)&&odir.magnitude<dir.magnitude){	
				lockOn = obj.transform;
			}
		}
		if(lockOn)
		target=lockOn.position;
	}

	protected override void Initialize ()
	{
		datab=GameObject.FindGameObjectWithTag("wepdata").GetComponent<WeaponDatabase>();
		base.Initialize ();
		isRateMaxed=false;
		isWeaponMaxed=false;
		weaponID=weapon.GetComponent<Bullet>().ID;
		tempWeaponID=0;
		tempRate=0;
		rateDiference=rateCap-fireRate;
		weaponDifference=weaponCap-weaponID;
	}

	public IEnumerator RevertRate(int str){
		yield return new WaitForSeconds(6.5f);
		RateDown(str);
		
	}

	public IEnumerator RevertWeapon(int str){
		yield return new WaitForSeconds(6.5f);
		WeaponDown(str);
		
	}
	
	public void LevelRateUp(int str){
		fireRate+=gameObject.floatUpgrade(rateDiference,rateUpgradeMax,(int)str);
		if (fireRate>=rateCap)
			isRateMaxed=true;
	}
	
	public void TempRateUp(int str){
		tempRate+=gameObject.floatUpgrade(rateDiference,rateUpgradeMax,(int)str);
		StartCoroutine(RevertRate(str));
	}
	
	void RateDown(int str){
		tempRate-=gameObject.floatUpgrade(rateDiference,rateUpgradeMax,(int)str);
	}

	public void LevelWeaponUp(int str){
		weaponID+=gameObject.intUpgrade(weaponDifference,weaponUpgradeMax,(int)str);
		if (weaponID>=weaponCap)
			isWeaponMaxed=true;
		QueryWepDB();
		//query db
	}
	
	public void TempWeaponUp(int str){
		tempWeaponID+=gameObject.intUpgrade(weaponDifference,weaponUpgradeMax,(int)str);
		StartCoroutine(RevertWeapon(str));
		QueryWepDB();
		//query db
	}
	
	void WeaponDown(int str){
		tempWeaponID-=gameObject.intUpgrade(weaponDifference,weaponUpgradeMax,(int)str);
		QueryWepDB();
		//query db
	}

	void QueryWepDB(){
		weapon = datab.weapons[Mathf.Clamp(weaponID+tempWeaponID,startWeapon.GetComponent<Bullet>().ID,weaponCap)];
		ArmWeapon(weapon.GetComponent<Bullet>().spawns);
	}
}
