﻿using UnityEngine;
using System.Collections;

public class EnemyWepHandler : WeaponHandler {

	protected override void UpdateTarget ()
	{
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		target = player.transform.position;
	}
}
