﻿using UnityEngine;
using System.Collections;

public static class Extensions {

	public static float floatUpgrade(this GameObject g, float difference, int max, int level){
		return (difference/max)*level;
	}

	public static int intUpgrade(this GameObject g, int difference, int max, int level){
		return (difference/max)*level;
	}

	                                // tempSpeed+=((SpeedDiff/upgradeInterval)*(int)str);
}
