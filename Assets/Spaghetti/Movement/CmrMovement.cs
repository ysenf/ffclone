﻿using UnityEngine;
using System.Collections;

public class CmrMovement : Movement {
	public GameObject player;
	
	private bool checkRange(){
		var xdif = Mathf.Abs (transform.position.x - target.x);
		var ydif = Mathf.Abs (transform.position.y - target.y);
		if ((xdif >= 1.32f) || (ydif >= 1.32f))
			return true;
		else
			return false;
	}
	
	override protected void UpdateTarget(){
		target=player.transform.position;
	}
	
	void UpdateSpeed(){
		CtrMovement plrHolder = player.transform.GetComponent<CtrMovement>();
//		Debug.Log (plrHolder);
		speed = (plrHolder.transform.GetComponent<CtrMovement>().Speed*0.7f);
	}
	
	override protected void MovementUpdates(){
		UpdateTarget();
		UpdateSpeed();
		if(checkRange())
		transform.position = Vector3.MoveTowards (transform.position, new Vector3 (target.x, target.y, -10f), speed * Time.deltaTime);
	}
	
	override protected void Initialize(){
		Debug.Log("start");
		//player = GameObject.FindGameObjectWithTag("Player");
		UpdateTarget();
		UpdateSpeed();
	}
}
