﻿using UnityEngine;
using System.Collections;

public class ChobMovement : Movement {
	private GameObject player;
	private bool bunkered;

	override protected void UpdateTarget(){
		target=player.transform.position;
	}
	
	override protected void MovementUpdates(){
		if(!bunkered){
		checkDistance();
		UpdateTarget();
		transform.position = Vector3.MoveTowards (transform.position, new Vector3 (target.x, target.y), speed * Time.deltaTime);
		}
	}
	
	override protected void Initialize(){
		bunkered=false;
		player = GameObject.FindGameObjectWithTag("Player");
		speed = 1.4f;
		UpdateTarget();
	}

	void checkDistance(){
		var dir = target-transform.position;
		if (dir.magnitude<=1.8f)
			bunkered=true;
	}


}
