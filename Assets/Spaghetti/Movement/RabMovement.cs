﻿using UnityEngine;
using System.Collections;

public class RabMovement : Movement {

	override protected void UpdateTarget(){
		GameObject Player = GameObject.FindGameObjectWithTag("Player");
		Vector3 dir = (Player.transform.position-transform.position);
		dir.Normalize();
		target=dir;
	}
	
	override protected void MovementUpdates(){
		transform.position += target*Time.deltaTime;
	}
	
	override protected void Initialize(){
		UpdateTarget();
	}
}
