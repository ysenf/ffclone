﻿using UnityEngine;
using System.Collections;

public abstract class Movement : MonoBehaviour {
	protected float speed;
	
	public float Speed
	{
		get
		{
			
			return speed;
		}
		set
		{
			speed = value;
		}
	}
	
	protected Vector3 target;
	
	public Vector3 Target
	{
		get
		{
			return target;
		}
		set
		{
			target = value;
		}
	}

	
	void Start () {
		Initialize();
	}

	void Update () {

		MovementUpdates();
	}
	protected abstract void MovementUpdates();

	protected abstract void Initialize();

	protected abstract void UpdateTarget();
}
