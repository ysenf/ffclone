﻿using UnityEngine;
using System.Collections;

public class CtrMovement : Movement {
	public float speedCap;
	public int upgradeMax;
	public float startSpeed;
	protected float speedDiff;
	protected float tempSpeed;
	public float TempSpeed
	{
		get
		{
			
			return tempSpeed;
		}
		set
		{
			tempSpeed = value;
		}
	}
	protected bool isMaxed;
	public bool IsMaxed
	{
		get
		{
			
			return isMaxed;
		}
		set
		{
			isMaxed = value;
		}
	}

	override protected void UpdateTarget(){
		target=Camera.main.ScreenToWorldPoint (Input.mousePosition);
	}

	override protected void MovementUpdates(){
//		Debug.Log (speed);
		UpdateTarget();
		transform.position = Vector3.MoveTowards (transform.position, new Vector3 (target.x, target.y), Mathf.Clamp(speed+tempSpeed,startSpeed,speedCap) * Time.deltaTime);
	}

	override protected void Initialize(){
		tempSpeed=0;
		isMaxed=false;
		speed = startSpeed;
		UpdateTarget();
		speedDiff=speedCap-speed;
	}

	public IEnumerator RevertSpeed(int str){
		yield return new WaitForSeconds(6.5f);
		LevelDown(str);

	}

	public void LevelUp(int str){
		speed+=gameObject.floatUpgrade(speedDiff,upgradeMax,(int)str);
		if (speed>=speedCap)
			isMaxed=true;
	}

	public void TempLevelUp(int str){
		tempSpeed+=gameObject.floatUpgrade(speedDiff,upgradeMax,(int)str);
		StartCoroutine(RevertSpeed(str));
	}

	void LevelDown(int str){
		tempSpeed-=gameObject.floatUpgrade(speedDiff,upgradeMax,(int)str);
	}
}
