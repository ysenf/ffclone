﻿using UnityEngine;
using System.Collections;

public class CtrHealth : Health {
	private bool invulnerable;
	private SpriteRenderer ren;
	private CtrMonitor monitor;

	protected override void Initialize ()
	{
		invulnerable=false;
		health=3;
		ren = gameObject.GetComponent<SpriteRenderer>();
		monitor = GameObject.FindGameObjectWithTag("monitor").GetComponent<CtrMonitor>();
	}

	protected override void OnHit (Collider2D other)
	{
		if(!invulnerable){
		if(other.transform.CompareTag("foe")==true||other.transform.CompareTag("fbullet")==true){
				invulnerable=true;
				TakeDamage(1);
				ReportHealth();
				StartCoroutine("DisableInvul");
				StartCoroutine("InvulBlink");
			}
		}
	}

	private void ReportHealth(){
		monitor.UpdateUI(health-1);
	}

	private IEnumerator DisableInvul(){
		yield return new WaitForSeconds(2.4f);
		invulnerable=false;
		StopCoroutine("InvulBlink");
		ren.color  =  new Color(ren.color.r,ren.color.g,ren.color.b,1f);
	}

	private IEnumerator InvulBlink(){
		ren.color  =  new Color(ren.color.r,ren.color.g,ren.color.b,0f);
		yield return new WaitForSeconds(0.1f);
		ren.color  =  new Color(ren.color.r,ren.color.g,ren.color.b,1f);
		yield return new WaitForSeconds(0.1f);
		StartCoroutine("InvulBlink");
	}

}
