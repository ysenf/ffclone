﻿using UnityEngine;
using System.Collections;


public class EnemyHealth : Health {
	public enum Size {Small = 30, Big = 55};

	public Size size;

	protected override void Initialize ()
	{
		health = (int)size;
	}

	protected override void OnHit (Collider2D other)
	{
		if(other.CompareTag("pbullet")){
			TakeDamage(other.GetComponent<Bullet>().damage);
		}
	}

	protected override void Death ()
	{
		//drop
		Destroy(gameObject);
	}
}
