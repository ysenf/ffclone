﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	protected int health;

	void Start () {
		Initialize();
	}


	void Update () {	
	}

	void OnTriggerEnter2D(Collider2D other){
		OnHit(other);
	}

	protected virtual void Initialize(){}

	protected virtual void OnHit(Collider2D other){
	}

	protected virtual void TakeDamage(int damage){
		if(damage>=health){
			health=0;
			Death();
		}
		else
			health-=damage;
	}

	protected virtual void Death(){
	}
}
