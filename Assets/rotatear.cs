﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class rotatear : MonoBehaviour {
	public GameObject player;
	public GameObject player2;
	public GameObject hand;
	protected List<Transform> weaponSpawn;
	// Use this for initialization
	void Start () {
		GameObject arm = (GameObject) Instantiate(hand,transform.position,Quaternion.identity);
		arm.transform.SetParent(transform);
		ArmWeapon (player.GetComponent<Bullet>().spawns);
	
	}
	
	// Update is called once per frame
	void Update () {

		/*if(Input.GetButtonDown("Fire1")){
			ArmWeapon (player2.GetComponent<Bullet>().spawns);
		}
		if(Input.GetButtonDown("Fire2")){
			ArmWeapon (player.GetComponent<Bullet>().spawns);
		}*/
		
	
	}

	protected void DestroySpawns(){
		foreach (Transform child in transform) {
			foreach (Transform spawns in child){
				if(spawns.tag=="spawn")
					GameObject.Destroy(spawns.gameObject);
			}
		}
	}

	protected void DestroyKids(){
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
	}

	protected void ArmWeapon(GameObject spawn){
		DestroyKids();
		GameObject arm = (GameObject) Instantiate(hand,transform.position,Quaternion.identity);
		arm.transform.SetParent(transform);
		GameObject preSpawn = (GameObject) Instantiate(spawn, new Vector3 (transform.position.x, (transform.position.y+0.5f), transform.position.z),Quaternion.identity);
		/*foreach (Transform child in preSpawn.transform){
			foreach (Transform gchild in child){
				gchild.SetParent(arm.transform);
			}
			child.SetParent(arm.transform);
		}*/
		//preSpawn.transform.SetParent(arm.transform);
		preSpawn.transform.SetParent(arm.transform);
		List<Transform> spawnTrans = new List<Transform>();
		foreach (Transform children in arm.transform){
			foreach (Transform gchild in children){
				spawnTrans.Add(gchild.transform);
			}
			spawnTrans.Add(children.transform);
		}
		weaponSpawn=spawnTrans;
		Debug.Log (weaponSpawn.Count);
	}


}
